Gradle and Javalin single page application example
--------------------------------------------------

- build.gradle (file, check for requirements)
- db.sh (file, use to make and remove database)
- required.front.sh (file, use to get front end requirements)
- db.drop.sql (file, sql commands to drop database)
- db.make.sql (file, sql commands to make database)

Requirements
------------

- Gradle version 6.2
- PostgreSQL version 9.5.19

Run and check
-------------

- build database (run ./db.sh make)
- get front end requirements (run ./required.front.sh)
- build app (run gradle build run)
- go to http://localhost:7000
