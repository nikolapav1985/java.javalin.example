CREATE DATABASE machinejavalin;
\c machinejavalin
CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    username VARCHAR(128),
    pass VARCHAR(128),
    firstname VARCHAR(128),
    lastname VARCHAR(128),
    levelof INTEGER,
    token VARCHAR(128),
    tokenexpiresat INTEGER
);
CREATE TABLE machines(
    id SERIAL PRIMARY KEY,
    machinetxtid VARCHAR(128) UNIQUE,
    status INTEGER,
    createdbyuser INTEGER,
    createdat INTEGER,
    active INTEGER
);

-- make a test user, pass md5 hash java of test, level 1 admin
INSERT INTO users (username,pass,firstname,lastname,levelof) VALUES ('test','CY9rzUYh03PK3k6DJie09g==','test','test',1);

-- make some test machines
-- status 1 stopped 2 running, active 1 true 0 false
INSERT INTO machines (machinetxtid,status,createdbyuser,createdat,active) VALUES ('testma',1,1,1585930326,1);
INSERT INTO machines (machinetxtid,status,createdbyuser,createdat,active) VALUES ('testmb',2,1,1585930326,1);
INSERT INTO machines (machinetxtid,status,createdbyuser,createdat,active) VALUES ('testmc',2,1,1585930326,0);

