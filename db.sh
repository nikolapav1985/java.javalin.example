#!/bin/bash
# to build database POSTGRES  type ./db.sh make
# to remove database POSTGRES type ./db.sh

CMP="make"

if [ "$1" == "$CMP" ];
then sudo -u postgres psql < db.make.sql
else sudo -u postgres psql < db.drop.sql
fi

