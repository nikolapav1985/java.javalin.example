#!/bin/bash

wget https://raw.githubusercontent.com/krampstudio/aja.js/master/src/aja.js
wget https://raw.githubusercontent.com/KoryNunn/crel/master/crel.js
wget https://raw.githubusercontent.com/jgallen23/routie/master/lib/routie.js
wget https://raw.githubusercontent.com/mvasilkov/rssi/derelict/rssi.js
mv aja.js src/main/resources/public
mv crel.js src/main/resources/public
mv routie.js src/main/resources/public
mv rssi.js src/main/resources/public
