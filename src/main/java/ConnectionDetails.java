import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ConnectionDetails{
        public static Connection c;
        public static Statement stmt;
        public ConnectionDetails(String details, String user, String pass){
            try{
                c=DriverManager.getConnection(details,user,pass);
                c.setAutoCommit(false);
                stmt=c.createStatement();
            }catch(Exception e){
            }
        }
}
