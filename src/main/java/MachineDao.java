import java.sql.ResultSet;

public class MachineDao{
    public static String error="{\"count\":\"0\"}";
    public static String success="{\"count\":\"1\"}"; 
    public static String indexPage="/velocity/index/index.vm";

    public String all(ConnectionDetails c){
        ResultSet rs;
        try{
            rs=c.stmt.executeQuery("select array_to_json(array_agg(row_to_json(t))) from (select * from machines) t");
            if(rs.next()){
                return rs.getString(1);
            } else {
                return error;
            }
        }catch(Exception e){
            return error;
        }
    }
    public String name(ConnectionDetails c, String name){
        ResultSet rs;
        try{
            rs=c.stmt.executeQuery("select row_to_json(t) from (select * from machines where machinetxtid='"+name+"') t");
            if(rs.next()){
                return rs.getString(1);
            } else {
                return error;
            }
        }catch(Exception e){
            return error;
        }
    }
    public String start(ConnectionDetails c, String name){
        int startAffected;
        try{
            startAffected=c.stmt.executeUpdate("update machines set status=2 where machinetxtid='"+name+"' and status=1");
            c.c.commit();
            if(startAffected>0){
                return success;
            } else {
                return error;
            }
        }catch(Exception e){
            return error;
        }
    }
    public String stop(ConnectionDetails c, String name){
        int stopAffected;
        try{
            stopAffected=c.stmt.executeUpdate("update machines set status=1 where machinetxtid='"+name+"' and status=2");
            c.c.commit();
            if(stopAffected>0){
                return success;
            } else {
                return error;
            }
        }catch(Exception e){
            return error;
        }
    }
    public String destroy(ConnectionDetails c, String name){
        int destroyAffected;
        try{
            destroyAffected=c.stmt.executeUpdate("delete from machines where machinetxtid='"+name+"'");
            c.c.commit();
            if(destroyAffected>0){
                return success;
            } else {
                return error;
            }
        }catch(Exception e){
            return error;
        }

    }
    public String create(ConnectionDetails c, String name){
        int createAffected;

        try{
            createAffected=c.stmt.executeUpdate("insert into machines (machinetxtid,status,createdbyuser,createdat,active) values ('"+name+"',1,1,"+(System.currentTimeMillis()/1000)+",1) on conflict (machinetxtid) do nothing");
            c.c.commit();
            if(createAffected>0){
                return success;
            } else {
                return error;
            }
        }catch(Exception e){
            return error;
        }
    }
}
