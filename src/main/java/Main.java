import io.javalin.Javalin;

public class Main{
        static ConnectionDetails c;
        static MachineDao mdao;

    public static void main(String args[]){
        Javalin app=Javalin.create(config -> {
            config.addStaticFiles("/public");
            config.enableDevLogging();
        }).start(7000);
        c=new ConnectionDetails("jdbc:postgresql://localhost:5432/machinejavalin","postgres","pass");
        mdao=new MachineDao();

        app.get("/", ctx -> ctx.render(mdao.indexPage)); // TODO get index page html ----- done
        app.get("/secure/machines", ctx -> {
            ctx.result(mdao.all(c)); 
        });
        app.get("/secure/name/:name", ctx -> {
            Name name=new Name(ctx.pathParam("name"));
            ctx.result(mdao.name(c,name.name));
        }); // need status all get here
        app.get("/secure/start/:name", ctx -> { // status 1 stopped 2 running
            Start start=new Start(ctx.pathParam("name")); // get search name
            ctx.result(mdao.start(c,start.name));
        });
        app.get("/secure/stop/:name", ctx -> {
            Stop stop=new Stop(ctx.pathParam("name")); // get search name
            ctx.result(mdao.stop(c,stop.name));
        });
        app.get("/secure/destroy/:name", ctx -> {
            Destroy destroy=new Destroy(ctx.pathParam("name")); // get search name
            ctx.result(mdao.destroy(c,destroy.name));
        });
        app.get("/secure/create/:name", ctx -> {
            Create create=new Create(ctx.pathParam("name")); // get search name
            ctx.result(mdao.create(c,create.name));
        });
    }
}
