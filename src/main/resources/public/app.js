// app
var machines=null;
var element="index";
var remove="item";
var templateDo=fmt("Name - #{name}, Status? -  #{status}");
var stoppedis=1;
var runningis=2;

aja()
    .url('/secure/machines')
    .on('success', function(data){
        machines=data;
    })
    .go();

function removeOfClass(classname){
    var elems=document.getElementsByClassName(classname);
    for(;elems[0];){
        elems[0].parentNode.removeChild(elems[0]);
    }
}

function listdo(){
    var i=0;
    var machine=null;
    removeOfClass(remove);
    if(machines == null){
    } else {
        for(;i<machines.length;i++){
            machine=machines[i];
            crel(document.getElementById(element),
                crel('div',{'class':'row item'},
                    crel('div',{'class':'col c12'},templateDo({
                        'name':machine['machinetxtid'],
                        'status':machine['status']
                        }))
            ));
        }
    }
}
function startdo(){
    var i=0;
    var machine=null;
    removeOfClass(remove);
    if(machines == null){
    } else {
        for(;i<machines.length;i++){
            machine=machines[i];
            crel(document.getElementById(element),
                crel('div',{'class':'row item'},
                    crel('div',{'class':'col c8'},templateDo({
                        'name':machine['machinetxtid'],
                        'status':machine['status']
                        })),
                    crel('div',{'class':'col c4'},
                        crel('a',{'class': 'btn btn-sm btn-b','href':'#startdoing/'+i},'Start'))
            ));
        }
    }
}
function stopdo(){
    var i=0;
    var machine=null;
    removeOfClass(remove);
    if(machines == null){
    } else {
        for(;i<machines.length;i++){
            machine=machines[i];
            crel(document.getElementById(element),
                crel('div',{'class':'row item'},
                    crel('div',{'class':'col c8'},templateDo({
                        'name':machine['machinetxtid'],
                        'status':machine['status']
                        })),
                    crel('div',{'class':'col c4'},
                        crel('a',{'class': 'btn btn-sm btn-b','href':'#stopdoing/'+i},'Stop'))
            ));
        }
    }
}
function startdoing(arrindex){
    machines[arrindex]['status'] = runningis;
    aja()
        .url('/secure/start/'+machines[arrindex]['machinetxtid'])
        .on('success', function(data){
            console.log(data);
        })
        .go();
}
function stopdoing(arrindex){
    machines[arrindex]['status'] = stoppedis;
    aja()
        .url('/secure/stop/'+machines[arrindex]['machinetxtid'])
        .on('success', function(data){
            console.log(data);
        })
        .go();
}

routie({
    'listdo': listdo,
    'startdo': startdo,
    'stopdo': stopdo,
    'startdoing/:arrindex': startdoing,
    'stopdoing/:arrindex': stopdoing
});
